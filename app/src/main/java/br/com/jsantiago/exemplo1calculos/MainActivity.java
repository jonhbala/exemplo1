package br.com.jsantiago.exemplo1calculos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private ListView mListaLv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListaLv = findViewById(R.id.lista);

        gerarListaDeCalculos();
    }

    private void gerarListaDeCalculos() {
        String[] menuItems = getResources().getStringArray(R.array.calculos);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, menuItems);

        mListaLv.setAdapter(adapter);

        mListaLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        levarOutraActivity(PorcentagemActivity.class);
                        break;
                }
            }
        });
    }

    private void levarOutraActivity(Class clazz){
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

}
