package br.com.jsantiago.exemplo1calculos;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class PorcentagemActivity extends AppCompatActivity {
    public static final int NUMERO = 0X100;
    public static final int PERCETAGEM = 0x105;

    private EditText mNumeroEt;
    private EditText mPercentagemEt;
    private TextView mResultTv;
    private Button mCalcularBtn;

    private int mNumero = 0, mPorcentagem = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_porcentagem);

        mNumeroEt = findViewById(R.id.number);
        mPercentagemEt = findViewById(R.id.percentage);
        mCalcularBtn = findViewById(R.id.btnCalcule);
        mResultTv = findViewById(R.id.result);

        mCalcularBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularPorcentagem();
            }
        });

        limparDadosAoDigitar(mNumeroEt, NUMERO);
        limparDadosAoDigitar(mPercentagemEt, PERCETAGEM);

    }

    private void limparDadosAoDigitar(EditText editText, final int tipo) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0)
                    switch (tipo) {
                        case NUMERO:
                            mNumero = Integer.parseInt(s.toString());
                            break;
                        case PERCETAGEM:
                            mPorcentagem = Integer.parseInt(s.toString());
                            break;
                    }
                mResultTv.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void calcularPorcentagem() {
        boolean validarDados = validarInputs();
        if (!validarDados) {
            mResultTv.setText(R.string.error_in_input_data);
            mResultTv.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            mResultTv.setVisibility(View.VISIBLE);
            return;
        }

        double percentualDecimal = (double) mPorcentagem / 100;
        double resultado = (double) mNumero * percentualDecimal;

        mResultTv.setText(String.format("%d%% de %d é igual à %.2f", mPorcentagem, mNumero, resultado));
        mResultTv.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
        mResultTv.setVisibility(View.VISIBLE);
    }

    private boolean validarInputs() {
        if (mNumeroEt.getText().length() == 0 || mPercentagemEt.getText().length() == 0)
            return false;


        return !(mNumero <= 0 || mPorcentagem <= 0);
    }


}
